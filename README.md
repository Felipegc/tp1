<h2>Primeira Parte</h2>

<h3>Bouncing Ball</h3>


O projeto consiste na representação de uma bola saltitante, saindo da origem de um plano cartesiano representado por uma caixa [-1,1]x[-1,1] com direção e velocidade definidas em conjunto com uma variável de gravidade. Tem o objetivo de aprender a implementar algumas classes de C++, explorar noções na programação orientada a objetos e vincular bibliotecas.

<h3>Requisitos</h3>


Os requisitos são basicamente a inclusão da biblioteca <i>iostream</i>, compilador g++ para a linguagem C++ e a versão do C++ utilizada foi a última: 6.3.0.

<h3>Compilação</h3>


A linha de compilação utilizada foi: g++ ball.cpp test-ball.cpp -o ball

<h3>Descrição de Arquivos</h3>


Temos 4 arquivos:<br>
<pre>	ball.cpp: Temos as definições de algumas variáveis e implementação das funções do arquivo ball.h.<br></pre>
<pre>	ball.h: É definido a classe <i>Ball</i> com seus atributos públicos e protegidos.<br></pre>
<pre>	simulation.h: Temos duas funções virtuais que implementam a interface do simulador.<br></pre>
<pre>	test-ball.cpp: Temos a instanciação da classe <i>Ball</i> e execução dos passos do programa com a definição da variável no plano.</pre>

<h3>Diagrama de Classes</h3>

<img src="diagram.png" alt="Diagrama">

<h3>Diagrama de Sequência</h3>

<img src="sdiagram.png" alt="DiagramaS">

<h3>Saída Texto</h3>

<pre>0.01 -0.00877778
0.02 -0.0284444
0.03 -0.059
0.04 -0.100444
0.05 -0.152778
0.06 -0.216
0.07 -0.290111
0.08 -0.375111
0.09 -0.471
0.1 -0.577778
0.11 -0.695444
0.12 -0.824
0.13 -0.824
0.14 -0.695444
0.15 -0.577778
0.16 -0.471
0.17 -0.375111
0.18 -0.290111
0.19 -0.216
0.2 -0.152778
0.21 -0.100444
0.22 -0.059
0.23 -0.0284444
0.24 -0.00877778
0.25 1.1534e-16
0.26 -0.00211111
0.27 -0.0151111
0.28 -0.039
0.29 -0.0737778
0.3 -0.119444
0.31 -0.176
0.32 -0.243444
0.33 -0.321778
0.34 -0.411
0.35 -0.511111
0.36 -0.622111
0.37 -0.744
0.38 -0.876778
0.39 -0.876778
0.4 -0.744
0.41 -0.622111
0.42 -0.511111
0.43 -0.411
0.44 -0.321778
0.45 -0.243444
0.46 -0.176
0.47 -0.119444
0.48 -0.0737778
0.49 -0.039
0.5 -0.0151111
0.51 -0.00211111
0.52 2.0957e-16
0.53 -0.00877778
0.54 -0.0284444
0.55 -0.059
0.56 -0.100444
0.57 -0.152778
0.58 -0.216
0.59 -0.290111
0.6 -0.375111
0.61 -0.471
0.62 -0.577778
0.63 -0.695444
0.64 -0.824
0.65 -0.824
0.66 -0.695444
0.67 -0.577778
0.68 -0.471
0.69 -0.375111
0.7 -0.290111
0.71 -0.216
0.72 -0.152778
0.73 -0.100444
0.74 -0.059
0.75 -0.0284444
0.76 -0.00877778
0.77 3.37385e-16
0.78 -0.00211111
0.79 -0.0151111
0.8 -0.039
0.81 -0.0737778
0.82 -0.119444
0.83 -0.176
0.84 -0.243444
0.85 -0.321778
0.86 -0.411
0.87 -0.511111
0.88 -0.622111
0.89 -0.744
0.89 -0.876778
0.88 -0.876778
0.87 -0.744
0.86 -0.622111
0.85 -0.511111
0.84 -0.411
0.83 -0.321778
0.82 -0.243444
0.81 -0.176
0.8 -0.119444
0.79 -0.0737778
</pre>

<h3>Saída Gráfico</h3>

<img src="graphic.png" alt="Grafico">

<h2>Segunda Parte</h2>

<h3>SpringMass</h3>


Consiste em um sistema massa-mola onde são criadas duas massas ligadas à uma mola e soltas em queda livre de acordo com as posições definidas, vetores de força e um plano XY [-1,1]x[-1,1].

<h3>Compilação</h3>


A linha de compilação utilizada foi: g++ springmass.cpp test-springmass.cpp -o springmass

<h3>Descrição de Arquivos</h3>


Temos 4 arquivos:<br>
<pre>	simulation.h: Temos duas funções virtuais que implementam a interface do simulador.<br></pre>
<pre>	springmass.h: Temos a criação das classes e funções a serem implementadas.</pre>
<pre>	springmass.cpp: Onde ficam as implementações das funções Spring(), Mass() e SpringMass().</pre>
<pre>	test-springmass.cpp: É definido os atributos para teste de execução do projeto.</pre>

<h3>Diagrama de Classes</h3>

<img src="diagram2.png" alt="Diagrama2">

<h3>Diagrama de Sequência</h3>

<img src="sdiagram2.png" alt="DiagramaS2">

<h3>Saída Texto</h3>

<pre>
-0.500746 0.182149
0.499254 -0.0178508
-0.502983 0.124997
0.497017 -0.0750034
-0.506712 0.0285424
0.493288 -0.171458
-0.511932 -0.107214
0.488068 -0.307214
-0.518644 -0.282271
0.481356 -0.482271
-0.526848 -0.49663
0.473152 -0.69663
-0.536543 -0.750291
0.463457 -0.950291
-0.547729 -0.750291
0.452271 -0.950291
-0.560407 -0.49303
0.439593 -0.69303
-0.574577 -0.275071
0.425423 -0.475071
-0.590238 -0.0964135
0.409762 -0.296414
-0.607391 0.0429424
0.392609 -0.157058
-0.626035 0.142997
0.373965 -0.0570034
-0.646171 0.203749
0.353829 0.00374915
-0.667798 0.2252
0.332202 0.0252
-0.690917 0.207349
0.309083 0.00734915
-0.715527 0.150197
0.284473 -0.0498034
-0.741629 0.0537424
0.258371 -0.146258
-0.769222 -0.0820135
0.230778 -0.282014
-0.798307 -0.257071
0.201693 -0.457071
-0.828884 -0.47143
0.171116 -0.67143
-0.860952 -0.725091
0.139048 -0.925091
-0.894511 -0.725091
0.105489 -0.925091
-0.929562 -0.46783
0.0704375 -0.66783
-0.966105 -0.249871
0.0338949 -0.449871
-0.966105 -0.0712135
-0.0041393 -0.271214
-0.929429 0.0681205
-0.0435313 -0.13188
-0.893158 0.167935
-0.0833288 -0.0320649
-0.855526 0.227818
-0.121765 0.027818
-0.814477 0.247199
-0.156785 0.0471991
-0.767596 0.225278
-0.185972 0.0252781
-0.712024 0.16091
-0.206468 -0.0390899
-0.644348 0.0524194
-0.214861 -0.147581
-0.560496 -0.102715
-0.207077 -0.302715
-0.455661 -0.308403
-0.17831 -0.508403
-0.324438 -0.570889
-0.123155 -0.770889
-0.161693 -0.900317
-0.0364794 -0.770889
0.0812492 -0.900317
0.130395 -0.428869
0.38864 -0.53923
0.361718 -0.0497979
0.696614 -0.195783
0.593623 0.311633
0.696614 0.128268
0.823013 0.653667
0.39158 0.431979
0.823013 0.975362
0.0917099 0.709404
0.598362 0.975362
-0.200637 0.953244
0.381235 0.649969
-0.481596 0.953244
0.175494 0.283969
-0.755324 0.709965
-0.023014 -0.125676
-0.755324 0.427378
-0.221718 -0.574631
-0.483095 0.107819
-0.422117 -0.574631
-0.211964 -0.247848
-0.623613 -0.121214
0.0343633 -0.633812
-0.849913 0.301904
0.25678 -0.633812
-0.849913 0.692223
0.48317 -0.255706
-0.595729 0.692223
0.716222 0.0759143
-0.334882 0.303275
0.954475 0.364539
-0.0688347 -0.128669
0.954475 0.613646
0.199954 -0.60013
0.716122 0.82631
0.471382 -0.60013
0.480187 0.82631
0.74523 -0.121176
0.24444 0.616564
0.74523 0.324543
0.0111498 0.367464
0.473653 0.730909
-0.210909 0.0834576
0.213306 0.730909
-0.423648 -0.232006
-0.0377203 0.331592
-0.633194 -0.582334
-0.285554 -0.102589
-0.837223 -0.96448
-0.52787 -0.568588
-0.837223 -0.96448
-0.76139 -0.568588
-0.633783 -0.568628
-0.76139 -0.0888835
-0.430371 -0.186739
-0.536108 0.376858
-0.231324 0.174298
-0.315189 0.821749
-0.0344987 0.509327
-0.0964932 0.821749
0.162042 0.808145
0.121919 0.370258
0.356681 0.808145
0.338428 -0.148715
0.548952 0.514199
0.552569 -0.733898
0.741198 0.181706
0.766687 -0.733898
0.933344 -0.184961
0.766687 -0.140744
0.933344 -0.556617
0.434924 0.44742
0.740897 -0.935862
-0.0148851 0.44742
0.550203 -0.935862
-0.462941 -0.172292
0.364587 -0.556022
-0.905919 -0.837744
0.183912 -0.217544
-0.905919 -0.837744
0.00891904 0.0838693
-0.4622 -0.164395
-0.162615 0.34766
-0.0150218 0.471332
-0.251721 0.640985
0.514584 0.471332
-0.251623 0.962501
0.514584 -0.162137
-0.246403 0.962501
-0.0991027 -0.834543
-0.243026 0.650928
-0.714633 -0.834543
-0.23841 0.309808
-0.714633 -0.155492
-0.229369 -0.0630276
-0.0959142 0.491845
-0.213097 -0.45427
0.530034 0.491845
-0.193554 -0.868134
0.530034 -0.156105
-0.177133 -0.868134
-0.102309 -0.847066
-0.539963 -0.555413
-0.102309 -0.847066
-0.539963 -0.399173
0.547967 -0.161232
0.198677 -0.293508
0.547967 0.474027
0.936219 -0.226911
-0.117899 0.474027
0.936219 -0.199668
-0.782459 -0.157917
0.203359 -0.212758
-0.782459 -0.830193
-0.520973 -0.263405
-0.115359 -0.830193
-0.520973 -0.355819
0.558299 -0.158525
0.198691 -0.492385
0.558299 0.468991
0.917107 -0.668875
-0.118497 0.468991
0.917107 -0.887547
-0.793075 -0.157183
</pre>

<h3>Saída Gráfico</h3>

<img src="graphic2.png" alt="Grafico2">

<h2>Terceira Parte</h2>

<h3>Spring Mass Graphics</h3>


Consiste em uma animação gráfica de duas massas ligadas à uma mola do exercício anterior em execução.

<h3>Compilação</h3>


A linha de compilação utilizada foi: make

<h3>Descrição de Arquivos</h3>


Temos 4 arquivos:<br>
<pre>	graphics.h: Temos a definição das classes Drawable() e Figure().<br></pre>
<pre>	graphics.cpp: É onde se encontram os métodos para a criação da saída gráfica.<br></pre>
<pre>	test-ball-graphics.cpp: É definido os atributos para teste de execução da primeira parte.<br></pre>
<pre>	test-springmass-graphics.cpp: É definido os atributos para teste de execução da segunda parte.</pre>

<h3>Diagrama de Sequência</h3>

<img src="sdiagram3.png" alt="DiagramaS3">

<h3>Saída Gráfico</h3>

<img src="Mgraphic.png" alt="GraficoMassa">
<img src="SMgraphic.png" alt="GraficoMolaMassa">
