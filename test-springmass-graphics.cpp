/** file: test-springmass-graphics.cpp
 ** brief: Tests the spring mass simulation with graphics
 ** author: Andrea Vedaldi
 **/

#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
/* ---------------------------------------------------------------- */
{
	private:
		Figure figure;
	public:
		SpringMassDrawable():SpringMass(),figure("Spring Mass"){
			figure.addDrawable(this);		
		}
		
		void draw(){
			int i=0;

			double px1 = this->getMass(i).getPosition().x;
			double px2 = this->getMass(i+1).getPosition().x;
			double py1 = this->getMass(i).getPosition().y;
			double py2 = this->getMass(i+1).getPosition().y;

			double radius = this->getMass(i).getRadius();

			figure.drawCircle(px1, py1, radius);
			figure.drawCircle(px2, py2, radius);
			figure.drawLine(px1, py1, px2, py2, 0.5);
		}

		void display(){
			figure.update();
		}

/* INCOMPLETE: TYPE YOUR CODE HERE */

} ;

int main(int argc, char** argv)
{
  glutInit(&argc,argv) ;

  SpringMassDrawable springmass ;

	const double mass = 0.05;
	const double radius = 0.02;
	const double naturalLength = 0.95;
	const double stiffness = 1.00 ;
	const double dt = 1.0/30;

	Mass m1(Vector2(-.5,0.2), Vector2(), mass, radius);
	Mass m2(Vector2(+.5,0), Vector2(), mass, radius);

	springmass.addMass(m1);
	springmass.addMass(m2);
	springmass.addSpring(0,1,naturalLength,stiffness);

	springmass.display();
	springmass.draw();

/* INCOMPLETE: TYPE YOUR CODE HERE */

  run(&springmass, 1/120.0) ;
}
